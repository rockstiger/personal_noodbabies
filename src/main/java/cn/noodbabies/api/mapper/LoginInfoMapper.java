package cn.noodbabies.api.mapper;

import cn.noodbabies.api.domain.LoginInfo;
import org.apache.ibatis.annotations.Param;

public interface LoginInfoMapper {

    int insert(LoginInfo entity);

    int updateByPrimaryKey(LoginInfo entity);

    LoginInfo selectByPrimaryKey(Long id);

    LoginInfo checkLogin(@Param("username") String username, @Param("password") String password);

}
