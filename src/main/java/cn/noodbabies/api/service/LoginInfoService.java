package cn.noodbabies.api.service;

import cn.noodbabies.api.domain.LoginInfo;

public interface LoginInfoService {
    /**
     * 登录校验
     * @param username
     * @param password
     * @return
     */
    void checkLogin(String username, String password);

    LoginInfo getAuthodById(Long id);
}
