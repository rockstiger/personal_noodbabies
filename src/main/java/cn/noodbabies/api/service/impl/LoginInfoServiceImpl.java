package cn.noodbabies.api.service.impl;

import cn.noodbabies.api.domain.LoginInfo;
import cn.noodbabies.api.mapper.LoginInfoMapper;
import cn.noodbabies.api.service.LoginInfoService;
import cn.noodbabies.api.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LoginInfoServiceImpl implements LoginInfoService {

    @Autowired
    private LoginInfoMapper loginInfoMapper;

    /**
     * 登录校验
     * @param username
     * @param password
     * @return
     */
    public void checkLogin(String username, String password) {
        LoginInfo loginInfo = loginInfoMapper.checkLogin(username, password);
        if(loginInfo == null){
            throw new RuntimeException("登录账号有误，请重新登录");
        }
        UserContext.setLoginUser(loginInfo);
    }

    public LoginInfo getAuthodById(Long id) {
        return loginInfoMapper.selectByPrimaryKey(id);
    }
}