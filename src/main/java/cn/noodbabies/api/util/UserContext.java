package cn.noodbabies.api.util;

import cn.noodbabies.api.domain.LoginInfo;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

public class UserContext {

    public static final String USER_IN_SESSION = "user_in_session";

    private static HttpSession getSession(){
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes.getRequest().getSession();
    }

    public static void setLoginUser(LoginInfo loginInfo){
        getSession().setAttribute(USER_IN_SESSION,loginInfo);
    }

    public static LoginInfo getLoginUser(){
        return (LoginInfo) getSession().getAttribute(USER_IN_SESSION);
    }
}
