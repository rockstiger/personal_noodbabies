package cn.noodbabies.api.util;


public class ResponseResult {

    private boolean success = true;

    private String message;

    public void setMessage(String message){
        this.success = false;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }
}
