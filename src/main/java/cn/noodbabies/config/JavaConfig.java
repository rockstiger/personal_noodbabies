package cn.noodbabies.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(basePackages="cn.noodbabies.api.mapper")
@ComponentScan(value = "cn.noodbabies")
public class JavaConfig {

    public static void main(String[] args) {
        SpringApplication.run(JavaConfig.class,args);
    }


}
