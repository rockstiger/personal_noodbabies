<ul id="menu" class="list-group">
    <li class="list-group-item">
        <a href="#" data-toggle="collapse" data-target="#usermanage_detail"><span>大标题1</span></a>
        <ul class="in" id="usermanage_detail">
            <li class=""><a href="/1.1.do">上传笔记</a></li>
            <li class=""><a href="/new_note_add">新笔记</a></li>
        </ul>
    </li>
    <li class="list-group-item">
        <a href="#" data-toggle="collapse" data-target="#permissionmanage_detail"><span>大标题2</span></a>
        <ul class="in" id="permissionmanage_detail">
            <li class="systemDictionary"><a href="/2.1.do"><span>小标题2.1</span></a></li>
            <li class="systemDictionaryItem"><a href="/2.2.do"><span>小标题2.2</span></a></li>
            <li><a href="/2.3.do"><span>小标题2.3</span></a></li>
            <li><a href="/2.4.do"><span>小标题2.4</span></a></li>
            <li><a href="/2.5.do"><span>小标题2.5</span></a></li>
            <li class="ipLog"><a href="/2.6.do"><span>小标题2.6</span></a></li>
        </ul>
    </li>
    <li class="list-group-item">
        <a href="#" data-toggle="collapse" data-target="#auditmanage_detail">
            <span>大标题3</span>
        </a>
        <ul class="in" id="auditmanage_detail">
            <li class="realAuth"><a href="/3.1.do">小标题3.1</a></li>
            <li class="vedioAuth"><a href="/3.2.do">小标题3.2</a></li>
            <li class="userFileAuth"><a href="/3.3.do">小标题3.3</a></li>
            <li class="bidrequest_publishaudit_list"><a href="/3.4.do">小标题3.4</a></li>
            <li class="bidrequest_audit1_list"><a href="/bidrequest_audit1_list.do">小标题3.5</a></li>
            <li class="bidrequest_audit2_list"><a href="/bidrequest_audit2_list.do">小标题3.6</a></li>
        </ul>
    </li>
</ul>

<#if currentMenu??>
<script type="text/javascript">
    $(".in li.${currentMenu}").addClass("active");
</script>
</#if>