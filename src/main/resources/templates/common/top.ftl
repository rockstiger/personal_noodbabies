<div class="navbar cm-navbar">
    <!--brand图片logo-->
    <!--<img class="logo" alt="Brand" src="/images">-->
    <span class="pageTitle">&nbsp;</span>
    <ul class="nav navbar-nav navbar-right cm-navbar-nav">
        <li>
            <p class="navbar-text text-info">${(loginfo.username)!""}</p>
        </li>
        <#if nologin??>
            <li><a href="/login.html">登录</a></li>

        <#else>
            <li><a href="/">安全退出</a></li>
            <li><a href="">个人设置</a></li>
        </#if>
    </ul>
</div>