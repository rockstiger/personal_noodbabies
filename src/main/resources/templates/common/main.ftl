<div>
    <div class="panel el-panel" style="padding: 50px">
        <div class="panel-title">
					<span class="pull-left">
						最新笔记列表
					</span>
            <div class="pull-right">
                <a class="btn btn-link " href="">
                    更多笔记
                </a>
            </div>
        </div>
        <div class="el-new-list">
            <table class="table el-table table-hover">

                <tbody>
                <#if noteList?size &gt; 0 >
                    <#list noteList as data>
                    <tr>
                        <td>
                            <a href="/note?id=${data.id}">
                                <span class="pull-left">${data.title} </span>
                            </a>
                        </td>
                        <td>
                            <span class="pull-right time-font">
                                ${data.updateTime?string('yyyy-MM-dd HH:mm:ss')}
                            </span>
                        </td>
                    </tr>
                    </#list>
                <#else>
                <tr>
                    <td colspan="2" align="center">
                        <p class="text-danger">目前暂时没有笔记分享</p>
                    </td>
                </tr>
                </#if>
                </tbody>

            </table>
            <div class="text-center"> 公告：无 </div>
        </div>
    </div>
</div>