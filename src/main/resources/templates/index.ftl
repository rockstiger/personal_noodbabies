<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>noodbabies后台管理系统</title>
    <link rel="stylesheet" href="/js/bootstrap-3.3.2-dist/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/css/core.css" type="text/css" />
    <script type="text/javascript" src="/js/jquery/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="/js/bootstrap-3.3.2-dist/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/jquery.bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/plugins/jquery.form.js"></script>
    <style type="text/css">
        body{
            background-color:  #eee;
        }

        .time-font{
            font-size: small;
            font-style: italic;
            font-family: Georgia, serif;
        }
    </style>
</head>
<body>
<div class="container">
		<#include "common/top.ftl"/>
    <div class="row">
        <div class="col-sm-3">
				<#include "common/menu.ftl" />
        </div>
        <div class="col-sm-9">
                <#include "common/main.ftl" />
        </div>
    </div>
</div>
</body>
</html>